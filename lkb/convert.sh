#! /bin/bash

LKBFOS=~/delphin/lkb_fos/lkb.linux_x86_64
LISPCOMMAND="${LKBFOS}"

{ 
    cat 2>&1 <<- LISP
  (format t "~%Read Grammar~%")
  (lkb::read-script-file-aux  "script")
  (format t "~%All Done!~%")
  #+allegro        (excl:exit)
  #+sbcl           (sb-ext:quit)
LISP
} | ${LISPCOMMAND}
