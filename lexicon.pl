% -*-trale-prolog-*-

%% macros for syllable and phoneme creation

ph(C) := (phoneme, core:C).
phl(C) := (phoneme, core:C, long:plus).
phs(C) := (phoneme, core:C, long:minus).

cvcs(O, N, C, R) := (syll, onset: @ph(O), nucleus: @phs(N),
		     coda: @ph(C), redup: R).
cvc(O, N, C, R) := (syll, onset: @ph(O), nucleus: @ph(N),
		    coda: @ph(C), redup: R).
cvcl(O, N, C, R) := (syll, onset: @ph(O), nucleus: @phl(N),
		     coda: @ph(C), redup: R).

cvs(O, N, R) := (syll, onset: @ph(O), nucleus: @phs(N), redup: R).
cvl(O, N, R) := (syll, onset: @ph(O), nucleus: @phl(N), redup: R).

vcs(N, C, R) := (syll, nucleus: @phs(N), coda: @phs(C), redup: R).
vcl(N, C, R) := (syll, nucleus: @phl(N), coda: @phl(C), redup: R).

vs(N, R) := (syll, nucleus: @phs(N), redup: R).
vl(N, R) := (syll, nucleus: @phl(N), redup: R).

cv(O, N, R) := (syll, onset: @ph(O), nucleus: @ph(N), redup: R).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                  reduplicative syllables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% it does not seem possible to prespecify *all* reduplicative syllables
% because there are some weird exceptions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                  plural formation in Nahuatl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% reduplication
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% make sure redup cases are taken care of
%% this does not apply to boundaries between redup syllables
%% because COPY is ne_list

(ne_list_red,
 copy:ne_list) *>
((ne_list_red,
  copy:(hd:(H,redup:plus), tl:T),
  hd:redup:plus,
  tl:(ne_list_red, copy:e_list, hd:H));
 (ne_list_red,
  hd:redup:minus)).

%% for reduplicating syllables
(ne_list_red,
 copy:hd:redup:plus) *>
(ne_list_red,
 copy:(hd:(H,(syll, onset:O,
	      nucleus:core:N, % only copy core, length can change
	      redup:plus)),
       tl:T),
 hd:(syll, onset:O, nucleus:core:N, redup:plus),
 tl:(ne_list_red, copy:e_list, hd:H)).

(ne_list_red, (copy:tl:ne_list, hd:redup:plus)) *>
(copy:tl:T, tl:tl:copy:T).

%% ensure copy is compiled for backwards parsing (plural->singular)
%% for non-reduplicating syllables
%% make sure hd = copy:hd
%% and that copy:tl = tl:copy

(ne_list_red, hd:redup:minus) *>
(ne_list_red, (copy:hd:H, hd:H)).

(ne_list_red, copy:hd:redup:minus) *>
(ne_list_red, (copy:hd:H, hd:H)).

(ne_list_red, hd:redup:minus, (copy:tl:ne_list;tl:hd:syll)) *>
(ne_list_red, copy:tl:T, tl:copy:T).

%% for reduplicating syllables
%% ensure tail percolation only if tl:ne_list

(ne_list_red,
 hd:redup:plus,
 tl:(hd:redup:plus,
     tl:((e_list);(hd:redup:minus)))) *>
(ne_list_red, tl:copy:e_list).

(ne_list_red, hd:redup:plus, tl:copy:e_list) *>
(ne_list_red, copy:hd:H, tl:hd:H).

(ne_list_red, hd:redup:plus, tl:hd:redup:plus, tl:tl:copy:ne_list) *>
(ne_list_red, copy:(hd:H,tl:T), tl:(hd:H,tl:copy:T)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% length of reduplication %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% minus percolates down, no matter what

(ne_list_red,
 hd:redup:minus,
 tl:hd:redup:minus)*>
(ne_list_red,
 hd:first:A,
 tl:hd:first:A).

%% we find a first redup syllable

(ne_list_red,
 hd:first:nil,
 tl:hd:redup:plus)*>
(ne_list_red,
 tl:hd:first:plus,
 tl:tl:hd:first:minus).

%% minus percolates all the way down

(ne_list_red,
 hd:first:minus,
 tl:hd:syll) *>
(ne_list_red,
 tl:hd:first:minus).

%% initialize the chain
%% basically, either the first syllable is first:nil, or first:plus

(noun,
 plural:hd:redup:minus)*>
(noun,
 plural:hd:first:nil).

(noun,
 plural:hd:redup:plus)*>
(noun,
 plural:(hd:first:plus,
	 tl:hd:first:minus)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% length when reduplicated %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% first reduplicant

(ne_list_red,
 hd:first:plus) *>
(ne_list_red,
 hd:nucleus:long:plus).

%% all other reduplicant

(ne_list_red,
 copy:hd:redup:plus,
 hd:first:minus) *>
(ne_list_red,
 hd:nucleus:long:minus).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%      singular marker     %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sg_tl_onset *> (noun,
		singular:append(A, [(cv, onset:core: @ph(ph_tl))])).
sg_tl_coda *> (noun,
	       singular:append(A, [(vc, coda:core: @ph(ph_tl))])).

sg_l_onset *> (noun,
	       singular:append(A, [(cv, onset:core: @ph(ph_l))])).

sg_n_coda *> (noun,
	      singular:append(A, [(vc, coda: @ph(ph_n))])).

sg_i *> (noun,
	 singular:append(A, [(cv, nucleus: @phs(ph_i))])).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%      plural marker     %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pl_h_coda *> (noun,
	      plural:copy:append(A, [(vc, coda: @ph(ph_h))])).

pl_in *> (noun,
	  plural:copy:append(A, [(vc, nucleus: @phs(ph_i),
				  coda: @ph(ph_n),
				  redup: minus)])).

pl_t_onset *> (noun,
	       plural:copy:append(A, [(cv, onset: @ph(ph_t),
				       redup: minus)])).

pl_qu_onset *> (noun,
		plural:copy:append(A, [(cv, onset: @ph(ph_qu),
					redup: minus)])).

pl_m_onset *> (noun,
	       plural:copy:append(A, [(cv, onset: @ph(ph_m),
				       redup: minus)])).

pl_e *> (noun,
	 plural:copy:append(A, [(syll, nucleus: @phs(ph_e),
				 redup: minus)])).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   red and non-red nouns  %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

noun_red *> (plural:(lo_ne_list_red)).

noun_non_red *> (plural:(ne_list_non_red,
			 copy:(hd:H, tl:T),
			   hd:H, tl:T)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%      number relations    %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% both singular and plural are identical
%% relation-1 in the paper
sg_x_pl_x_relation *>
(singular:A,
 plural:copy:A).

%% both singular and plural are identical upto the last syllable
%% relation-3 in the paper
sg_xo_pl_xo_relation *>
(noun,
 singular:append(A, [(onset:O)]),
 plural:copy:append(A, [(onset:O)])).

%% both singular and plural are identical upto the last syllable
%% relation-3 in the paper
sg_xs_pl_xs_relation *>
(noun,
 singular:append(A, [syll]),
 plural:copy:append(A, [syll])).

%% the plural contains the singular plus one syllable
%% relation-4 in the paper
sg_x_pl_xs_relation *>
(noun,
 singular:A,
 plural:copy:append(A, [syll])).

%% different coda in the singular and plural but same number of syllables
%% relation-5 in the paper
sg_xc_pl_xc_relation *>
(noun,
 singular:append(A, [(onset:O, nucleus:core:N, redup:R)]),
 plural:copy:append(A, [(onset:O, nucleus:(core:N, long:minus), redup:R)])).

%% onset becomes coda
%% relation-6 in the paper
sg_onset_pl_coda_relation *>
(noun,
 singular:append(A, [(onset:O1, nucleus:N),
		     (onset:O2)]),
 plural:copy:append(A, [(onset:O1, nucleus:N, coda:O2), syll])).

%% final coda in the singular disappears
%% relation-7 in the paper
sg_coda_pl_syll_relation *>
(noun,
 singular:append(A, [(syll, onset:O, nucleus:N, first:F, redup:R)]),
 plural:copy:append(A, [(syll, onset:O, nucleus:N, first:F, redup:R),
			syll])).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%          nouns           %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% some lexical examples from the paper

%% sg_x_pl_x_class

n_pilpil --->(sg_x_pl_x_class_red,
	      singular:[@cvcs(ph_p,ph_i,ph_l, plus),
			@cvcl(ph_p,ph_i,ph_l, plus)]).

n_pilpil2 --->(sg_x_pl_x_class_red,
	       plural:[@cvcl(ph_p,ph_i,ph_l, plus),
		       @cvcs(ph_p,ph_i,ph_l, plus),
		       @cvcs(ph_p,ph_i,ph_l, plus),
		       @cvcl(ph_p,ph_i,ph_l, plus)]).

%% sg_i_pl_eh_class

n_micqui --->(sg_i_pl_eh_class_red,
	      singular:[@cvcs(ph_m, ph_i, ph_c, plus),
			@cvs(ph_qu, ph_i, minus)]).

n_teomicqui --->(sg_i_pl_eh_class_non_red,
		 singular:[@cvs(ph_t, ph_e, minus),
			   (syll,
			    nucleus:(core:ph_o,
				     long:plus),
			    redup:minus),
			   @cvcs(ph_m, ph_i, ph_c, plus),
			   @cvs(ph_qu, ph_i, minus)]).

%% sg_n_pl_meh_class

n_tepezolin --->(sg_n_pl_meh_class_non_red,
		 singular:[@cvs(ph_t, ph_e, minus),
			   @cvl(ph_p, ph_e, minus),
			   @cvl(ph_z, ph_o, minus),
			   @cvcs(ph_l, ph_i, ph_n, minus)]).

%% sg_itl_pl_meh_class

n_atemitl --->(sg_itl_pl_meh_class_non_red,
	       singular:[@vl(ph_a, minus),
			 @cvs(ph_t, ph_e, minus),
			 @cvcs(ph_m, ph_i, ph_tl, minus)]).

%% sg_in_pl_tin_class

n_chapolin --->(sg_in_pl_tin_class_non_red,
		singular:[@cvl(ph_ch, ph_a, minus),
			  @cvs(ph_p, ph_o, minus),
			  @cvcs(ph_l, ph_i, ph_n, minus)]).

n_citlalin --->(sg_in_pl_tin_class_red,
		singular:[@cvl(ph_c, ph_i, plus),
			  @cvs(ph_tl, ph_a, minus),
			  @cvcs(ph_l, ph_i, ph_n, minus)]).

%% sg_tli_pl_tin_class

n_cocohtli --->(sg_tli_pl_tin_class_non_red,
	       singular:[@cvl(ph_c, ph_o, minus),
			  @cvcs(ph_o, ph_o, ph_h, minus),
			  @cvs(ph_tl, ph_i, minus)]).

%% sg_li_pl_tin_class

n_nahualli --->(sg_li_pl_tin_class_red,
		singular:[@cvl(ph_n, ph_a, plus),
			  @cvs(ph_h, ph_u, minus),
			  @vcs(ph_a, ph_l, minus),
			  @cvs(ph_l, ph_i, minus)]).

%% sg_x_pl_meh_class

n_chichi --->(sg_x_pl_meh_class_non_red,
	      singular:[@cvs(ph_ch, ph_i, minus),
			@cvs(ph_ch, ph_i, minus)]).

%% sg_x_pl_queh_class

n_michhuah --->(sg_x_pl_queh_class_non_red,
		singular:[@cvcs(ph_m, ph_i, ph_ch, minus),
			  @cvs(ph_h, ph_u, minus),
			  @vcs(ph_a, ph_h, minus)]).

%% sg_tl_pl_h_class

n_coyametl --->(sg_tl_pl_h_class_red,
		singular:[@cvs(ph_c, ph_o, plus),
			  @cvs(ph_y, ph_a, minus),
			  @cvcs(ph_m, ph_e, ph_tl, minus)]).

n_tecolotl --->(sg_tl_pl_h_class_red,
		singular:[@cvs(ph_t, ph_e, plus),
			  @cvs(ph_c, ph_o, minus),
			  @cvcs(ph_l, ph_o, ph_tl, minus)]).

%% sg_tl_pl_meh_class

n_tecolotl2 --->(sg_tl_pl_meh_class_non_red,
		 singular:[@cvs(ph_t, ph_e, plus),
			   @cvs(ph_c, ph_o, minus),
			   @cvcs(ph_l, ph_o, ph_tl, minus)]).

%% sg_tli_pl_meh_class

n_tlapantli --->(sg_tli_pl_meh_class_non_red,
		 singular:[@cvs(ph_tl, ph_a, minus),
			   @cvcs(ph_p, ph_a, ph_n, minus),
			   @cvs(ph_tl, ph_i, minus)]).



%% sg_in_pl_meh_class_red

n_quimichin --->(sg_n_pl_meh_class_non_red,
		 singular:[@cvs(ph_q,ph_i, minus),
			   @cvs(ph_m,ph_i, minus),
			   @cvcs(ph_ch, ph_i, ph_n, minus)]).

n_quimichin2 --->(sg_n_pl_meh_class_non_red,
		  plural:[@cvs(ph_q,ph_i, minus),
			  @cvs(ph_m,ph_i, minus),
			  @cvs(ph_ch, ph_i, minus),
			  @cvcs(ph_m, ph_e, ph_h, minus)]).




