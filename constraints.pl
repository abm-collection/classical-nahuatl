% -*-trale-prolog-*-
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   $RCSfile: constraints.pl,v $
%%  $Revision: 1.4 $
%%      $Date: 2019/03/19 20:04:51 $
%%     Author: 
%%    Purpose: AbM
%%   Language: Trale
%      System: TRALE 2.7.5 (release ) under Sicstus 3.10.1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- multifile if/2.

% *****************************
%   list manipulation utilities
% *****************************

% append(+,+,-)
% This append assumes that the first or the third argument
% are known to be non_empty or empty lists. 
%

fun append(+,+,-).
append(X,Y,Z) if 
   when( (X=(e_list;ne_list);
          Z=(e_list;ne_list)) 
       , undelayed_append(X,Y,Z)
       ).

undelayed_append([],L,L) if true.
undelayed_append([H|T1],L,[H|T2]) if append(T1,L,T2).

% lo_ne_list_red(-)
% makes all list elements be other ne_list_red
% this is necessary because trale is lazy.

fun lo_ne_list_red(-).
lo_ne_list_red(X) if
when( (X=(e_list;ne_list) ),
      und_lo_ne_list_red(X)).

und_lo_ne_list_red(X) if (X=e_list).
und_lo_ne_list_red(X) if (X=[H|Y]),  (X=ne_list_red), lo_ne_list_red(Y).

%% Type negation - delay until type T and then die.
%% not necessary but still
not(Type) macro not_type(a_ Type).
fun not_type(+,-).
( not_type((a_ Type),FS) if
  when(FS=Type,
       prolog(fail)) ) :-
    type(Type).
