% -*-trale-prolog-*-

% use ghostview for drawing signatures
graphviz_option(ps,gv).
%graphviz_option(svg,squiggle).

% Load tree output

:- [trale_home(tree_extensions)].

hidden_feat(copy). % hide copy feature

>>> onset.
onset <<< nucleus.
nucleus <<< coda.
long <<< coda.

>>> singular.
singular <<< plural.
plural <<< phon.

% specify signature file
signature(signature).

% load lexicon
:- [lexicon].

% load relational constraints for rules
:- [constraints].



